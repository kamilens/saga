package az.ingress.stocks.dto;

import az.ingress.stocks.common.dto.AccountDto;
import az.ingress.stocks.model.orders.OrderStatus;
import java.util.Date;
import lombok.Data;

@Data
public class OrderDto {

    private Long id;
    private Double maxPrice;
    private Double filledPrice;
    private String stockId;
    private Long numberOfShares;
    private String type;
    private OrderStatus status;
    private AccountDto account;
    private Date created;
    private Date updated;
}
