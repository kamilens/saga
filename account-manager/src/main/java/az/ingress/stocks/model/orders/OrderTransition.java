package az.ingress.stocks.model.orders;

import az.ingress.stocks.dto.OrderDto;

public interface OrderTransition {

    String getName();

    OrderStatus getStatus();

    //This should do required pre processing
    void applyProcessing(OrderDto order);
}
