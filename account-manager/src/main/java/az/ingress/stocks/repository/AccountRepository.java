package az.ingress.stocks.repository;

import az.ingress.stocks.common.model.Account;
import az.ingress.stocks.common.model.user.User;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {

    Optional<Account> findByUser(User user);
}
