package az.ingress.stocks.repository;

import az.ingress.stocks.common.model.Holding;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface HoldingRepository extends CrudRepository<Holding, Long> {

    public Optional<Holding> findByAccountIdAndStockId(Long accountId,String stockId);
}
