package az.ingress.stocks.repository;

import az.ingress.stocks.model.orders.Order;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderRepository extends PagingAndSortingRepository<Order, Long>,
        JpaSpecificationExecutor<Order> {

}
