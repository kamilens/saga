package az.ingress.stocks.service;

import az.ingress.stocks.common.dto.GenericSearchDto;
import az.ingress.stocks.common.dto.StockDto;
import az.ingress.stocks.common.model.PStock;
import az.ingress.stocks.common.repository.redis.PStocksRedisRepository;
import az.ingress.stocks.common.repository.search.SearchSpecification;
import az.ingress.stocks.repository.StocksRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class StocksService {

    private final ModelMapper mapper;
    private final StocksRepository stocksRepository;
    private final PStocksRedisRepository pStocksRedisRepository;

    /**
     * Lists all stocks fpr given search filter.
     *
     * @param filter : the search filter
     * @param pageable : the pageable
     * @return: the results found
     */
    @Transactional
    public Page<StockDto> findAll(GenericSearchDto filter, Pageable pageable) {
        return stocksRepository.findAll(new SearchSpecification<>(filter.getCriteria()), pageable)
                .map(p -> mapper.map(p, StockDto.class));
    }

    public Iterable<PStock> findAllPreferential() {
        return pStocksRedisRepository.findAll(); //I don't care about pagination, just do it work
    }
}
