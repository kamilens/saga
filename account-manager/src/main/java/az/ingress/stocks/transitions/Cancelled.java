package az.ingress.stocks.transitions;

import az.ingress.stocks.model.orders.OrderStatus;
import az.ingress.stocks.model.orders.OrderTransition;
import az.ingress.stocks.dto.OrderDto;
import org.springframework.stereotype.Service;

@Service
public class Cancelled implements OrderTransition {

    public static final String NAME = "cancelled";

    @Override
    public String getName() {
        return null;
    }

    @Override
    public OrderStatus getStatus() {
        return OrderStatus.CANCELLED;
    }

    @Override
    public void applyProcessing(OrderDto order) {
        //Do processing here
    }
}
