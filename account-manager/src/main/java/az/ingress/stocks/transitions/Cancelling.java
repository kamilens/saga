package az.ingress.stocks.transitions;

import az.ingress.stocks.model.orders.OrderStatus;
import az.ingress.stocks.model.orders.OrderTransition;
import az.ingress.stocks.dto.OrderDto;
import org.springframework.stereotype.Service;

@Service
public class Cancelling implements OrderTransition {

    public static final String NAME = "cancelling";

    @Override
    public String getName() {
        return Cancelling.NAME;
    }

    @Override
    public OrderStatus getStatus() {
        return OrderStatus.CANCELLING;
    }

    @Override
    public void applyProcessing(OrderDto order) {
        //Do processing here
    }
}
