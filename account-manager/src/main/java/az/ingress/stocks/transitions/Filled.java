package az.ingress.stocks.transitions;

import az.ingress.stocks.model.orders.OrderStatus;
import az.ingress.stocks.model.orders.OrderTransition;
import az.ingress.stocks.service.AccountService;
import az.ingress.stocks.dto.OrderDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Filled implements OrderTransition {

    public static final String NAME = "filled";

    private final AccountService accountService;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public OrderStatus getStatus() {
        return OrderStatus.FILLED;
    }

    @Override
    public void applyProcessing(OrderDto order) {
        //Do processing here
        accountService.applyOrder(order);
    }
}
