package az.ingress.stocks.common.repository.redis;

import az.ingress.stocks.common.model.Account;
import az.ingress.stocks.common.model.user.User;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRedisRepository extends CrudRepository<Account, Long> {

    Optional<Account> findByUser(User user);
}
