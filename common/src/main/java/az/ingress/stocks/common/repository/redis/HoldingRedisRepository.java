package az.ingress.stocks.common.repository.redis;

import az.ingress.stocks.common.model.Holding;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HoldingRedisRepository extends CrudRepository<Holding, Long> {

    List<Holding> findByStockId(String stockId);
    List<Holding> findByAccountId(Long accountId);
}
