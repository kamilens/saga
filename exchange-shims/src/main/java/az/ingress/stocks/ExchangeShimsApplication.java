package az.ingress.stocks;

import az.ingress.stocks.common.dto.CreateOrderDto;
import az.ingress.stocks.client.ReactiveStocksClient;
import az.ingress.stocks.common.config.LoggingTcpConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@Import({LoggingTcpConfiguration.class})
public class ExchangeShimsApplication implements CommandLineRunner {

    @Autowired
    private ReactiveStocksClient reactiveStocksClient;

    @Autowired
    public KafkaTemplate<String, CreateOrderDto> createOrderTemplate;

    @Value("${kafka.topics.orders.create}")
    private String createOrdersTopic;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(ExchangeShimsApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        reactiveStocksClient.receiveStockPrices();
    }
}
