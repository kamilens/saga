package com.nyse.hooks.client;

import com.nyse.dto.ClientOrderStatusDto;
import com.nyse.dto.OrderDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
@RequiredArgsConstructor
public class HookManager {

    private final RestTemplate restTemplate;

    public void sendResponse(OrderDto orderDto) {
        log.trace("Sending response:" + orderDto);
        ClientOrderStatusDto clientOrderStatusDto = ClientOrderStatusDto.builder()
                .status(orderDto.getStatus().name())
                .externalId(orderDto.getExternalId())
                .id(orderDto.getId())
                .filledPrice(orderDto.getFilledPrice())
                .build();
        log.trace("Sending response {} to {}",clientOrderStatusDto,orderDto.getHookURL());
        ResponseEntity<Void> response
                = restTemplate
                .postForEntity(orderDto.getHookURL(), clientOrderStatusDto, Void.class);
        if (response.getStatusCode().is2xxSuccessful()) {
            log.trace("Response sent successfully");
        } else {
            log.error("Something is wrong" + response); //@ToDo: handle it properly
        }
    }
}
